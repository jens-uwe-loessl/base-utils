## Changelog

### 1.8.0 (04/13/2020)
- add  Generator function to create UUIDS

### 1.7.0 (04/13/2020)
- add AsynUtils with an async await approach of window.timeOut 