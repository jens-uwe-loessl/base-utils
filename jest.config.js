module.exports = {
	'transform': {
		'.*\\.(ts)$': 'ts-jest'
	},
	'moduleFileExtensions': [
		'ts',
		'js',
	],
	'testRegex': '/test/.*\\.spec.(ts|js)$',
	'collectCoverageFrom': [
		'src/**/*.{js,ts}',
		'!**/*.d.ts'
	],
	'coveragePathIgnorePatterns': [
		'/node_modules/',
		'/test/.*\\.(ts|js)$',
	],
	'coverageThreshold': {
		'global': {
			'branches': 75,
			'functions': 75,
			'lines': 75,
			'statements': 70
		}
	},
	'testEnvironment': 'jsdom',
	'testURL': 'http://localhost/'
};