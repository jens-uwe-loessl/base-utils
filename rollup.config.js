import typescript from 'rollup-plugin-typescript2';

module.exports = {
    input: 'src/index.ts',
    output: [{
        file: 'dist/cjs/index.js',
        format: 'cjs'
    }, {
        file: 'dist/es/index.js',
        format: 'es'
    }],
    plugins: [
        typescript()
    ]
};