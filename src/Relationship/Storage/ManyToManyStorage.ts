export class ManyToManyStorage {
	private readonly _columns: string[];
	private _storage: number[][] = [];

	constructor(columns: string[]) {
		if (columns.length !== 2) {
			throw new Error('for a many to many relationship store two columns are needed');
		}
		this._columns = columns;
	}

	/**
	 * detaches two linked objects from the local storage
	 * @param firstClassName
	 * @param idInstance
	 * @param idLinkedInstance
	 */
	public detach(firstClassName: string, idInstance: number, idLinkedInstance: number): void {
		const positionIndex = this._columns.indexOf(firstClassName);
		const positionLinkedInstance = positionIndex ? 0 : 1;

		const searchArray = new Array(2);

		searchArray[positionLinkedInstance] = idLinkedInstance;
		searchArray[positionIndex] = idInstance;

		this._storage = this._storage.filter((storedPair) => {
			return idInstance !== storedPair[positionIndex] || storedPair[positionLinkedInstance] !== idLinkedInstance;
		});
	}

	/**
	 * returns all linked ids from the storage
	 * @param firstClassName
	 * @param idInstance
	 */
	public getLinkedIds(firstClassName: string, idInstance: number): number[] {
		const positionIndex = this._columns.indexOf(firstClassName);

		if (!~positionIndex) {
			throw new Error('trying to link an instance from the storage with a wrong column: ' + firstClassName);
		}

		const positionLinkedInstance = positionIndex ? 0 : 1;

		return this._storage.reduce((acc, act) => {
			if (act[positionIndex] === idInstance) {
				acc.push(act[positionLinkedInstance]);
			}
			return acc;
		}, [] as number[]);
	}

	/**
	 * links to instances in the store
	 * @param firstClassName
	 * @param idInstance
	 * @param idLinkedInstance
	 */
	public link(firstClassName: string, idInstance: number, idLinkedInstance: number): void {
		const linkedIds = this.getLinkedIds(firstClassName, idInstance);
		const positionIndex = this._columns.indexOf(firstClassName);
		const positionLinkedInstance = positionIndex ? 0 : 1;

		if (!~linkedIds.indexOf(idLinkedInstance)) {
			const tmpArray = new Array(2);

			tmpArray[positionLinkedInstance] = idLinkedInstance;
			tmpArray[positionIndex] = idInstance;

			this._storage.push(tmpArray);
		}
	}
}