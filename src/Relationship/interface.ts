/**
 * object that has an id property
 */
export interface IIdentifiable {
	Id: number;
}

/**
 * repository of a x to many relationship
 */
export interface ILinkableRepositoryMany<T> extends ILinkableRepositoryBase<T> {
	getCollectionByIds(linkedIds: number[]): Promise<T[]>;
}

/**
 * repository for a x to one relationship
 */
export interface ILinkableRepositoryOne<T> extends ILinkableRepositoryBase<T> {
	getById(id: number): Promise<T>;
}

/**
 * base of the linkable repositories
 */
interface ILinkableRepositoryBase<T> {
	createOrUpdate(instance: T): Promise<T>;
}