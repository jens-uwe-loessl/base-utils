import {ManyToManyStorage} from '../Storage/ManyToManyStorage';

/**
 * @class RelationshipStorageFactory
 * manages the storage for the relationships, and creates new one when needed
 */
export class RelationshipStorageFactory {
	private static _instance?: RelationshipStorageFactory;

	public static get Instance() {
		return this._instance || (this._instance = new RelationshipStorageFactory());
	}

	private _manyToManyStorages: { [key: string]: ManyToManyStorage } = {};

	private constructor() {}

	/**
	 * returns a storage ore creates a new one for a pair of two classes
	 * @param className
	 * @param linkedClassName
	 */
	public getManyToManyStorage(className: string, linkedClassName: string) {
		const columns = [className, linkedClassName].sort((a, b) => a < b ? -1 : a === b ? 0 : 1);
		const key = columns.join('-');

		return this._manyToManyStorages[key] || (this._manyToManyStorages[key] = new ManyToManyStorage(columns));
	}
}