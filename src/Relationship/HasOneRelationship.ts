import {RelationshipStorageFactory} from './Factory/RelationshipStorageFactory';
import {ManyToManyStorage} from './Storage/ManyToManyStorage';

import {UnsupportedMethodCallError} from '../Error/UnsupportedMethodCallError';
import {IIdentifiable, ILinkableRepositoryOne} from './interface';

/**
 * @class HasOneRelationship
 * its a helper to link on class to another
 * it can be used when on class has one instance linked, like in a one to many relationship
 */
export class HasOneRelationship<T> {
	private readonly _className: string;
	private _instance: IIdentifiable;
	private readonly _storage: ManyToManyStorage;
	private readonly _repository: ILinkableRepositoryOne<T>;

	/**
	 * @constructor
	 * @param instance
	 * @param repository
	 * @param actualClassName
	 * @param linkedClassName
	 * @param linkedItemId
	 */
	constructor(
		instance: IIdentifiable, repository: ILinkableRepositoryOne<T>, actualClassName: string,
		linkedClassName: string, linkedItemId?: number
	) {
		this._className = actualClassName;
		this._instance = instance;
		this._repository = repository;
		this._storage = RelationshipStorageFactory.Instance.getManyToManyStorage(actualClassName, linkedClassName);

		if (linkedItemId !== undefined) {
			this.link(linkedItemId, false);
		}
	}

	/**
	 * unlink the linked instances
	 * @param id
	 * @param sendRequest
	 * @return Promise<void>
	 */
	public async detach(id: number, sendRequest: boolean = true): Promise<void> {
		if (sendRequest) {
			try {
				this._storage.detach(this._className, this._instance.Id, id);

				const instanceDetach = await this._repository.getById(id);
				await this._repository.createOrUpdate(instanceDetach);
			} catch (e) {
				await this.link(id, false);
				throw  e;
			}
			return;
		}

		this._storage.detach(this._className, this._instance.Id, id);
	}

	/**
	 * links two instances
	 * @param id
	 * @param sendRequest
	 */
	public async link(id: number, sendRequest: boolean = true): Promise<void> {
		const linkedIds = this._storage.getLinkedIds(this._className, this._instance.Id);

		if (linkedIds.length === 1 && linkedIds[0] === id) {
			return; // do nothing because of it is already linked

		} else if (linkedIds.length > 0) {
			throw new UnsupportedMethodCallError('HasOneRelationship - can not link when there is already a linked instance: '
				+ this._className + ' newId: ' + id + ' oldId: ' + linkedIds[0]);
		}

		if (sendRequest) {
			try {
				this._storage.link(this._className, this._instance.Id, id);

				const instanceLink = await this._repository.getById(id);
				await this._repository.createOrUpdate(instanceLink);
			} catch (e) {
				await this.detach(id, false);
				throw e;
			}
			return;
		}

		this._storage.link(this._className, this._instance.Id, id);
	}

	/**
	 * returns an array of the of the linked item
	 */
	public async get(): Promise<T> {
		return await this._repository.getById(this.LinkedId);
	}

	/**
	 * returns the list of linked ids
	 */
	public get LinkedId() {
		return this._storage.getLinkedIds(this._className, this._instance.Id)[0];
	}
}