import {RelationshipStorageFactory} from './Factory/RelationshipStorageFactory';
import {ManyToManyStorage} from './Storage/ManyToManyStorage';

import {IIdentifiable, ILinkableRepositoryMany} from './interface';

/**
 * @class HasManyRelationship
 * its a helper to link on class to another
 * it can be used when on class has multiple instances linked, like in a many to many relationship
 */
export class HasManyRelationship<T> {
	private readonly _className: string;
	private _instance: IIdentifiable;
	private readonly _storage: ManyToManyStorage;
	private readonly _repository: ILinkableRepositoryMany<T>;

	/**
	 * @constructor
	 * @param instance
	 * @param repository
	 * @param actualClassName
	 * @param linkedClassName
	 * @param linkedItemIds
	 */
	constructor(
		instance: IIdentifiable, repository: ILinkableRepositoryMany<T>, actualClassName: string,
		linkedClassName: string, linkedItemIds: number[] = []
	) {
		this._className = actualClassName;
		this._instance = instance;
		this._repository = repository;
		this._storage = RelationshipStorageFactory.Instance.getManyToManyStorage(actualClassName, linkedClassName);

		for (const id of linkedItemIds) {
			this.link(id, false);
		}
	}

	/**
	 * unlink the linked instances
	 * @param id
	 * @param sendRequest
	 * @return Promise<void>
	 */
	public async detach(id: number, sendRequest: boolean = true): Promise<void> {
		if (sendRequest) {
			try {
				this._storage.detach(this._className, this._instance.Id, id);

				const instanceDetach = await this._repository.getCollectionByIds([id]);
				await this._repository.createOrUpdate(instanceDetach[0]);
			} catch (e) {
				await this.link(id, false);
				throw e;
			}
			return;
		}

		this._storage.detach(this._className, this._instance.Id, id);
	}

	/**
	 * detaches all linked instances
	 */
	public async detachAll(): Promise<void> {
		const linkedIds = this._storage.getLinkedIds(this._className, this._instance.Id);

		for (const id of linkedIds) {
			await this.detach(id, false);
		}
	}

	/**
	 * links two instances
	 * @param id
	 * @param sendRequest
	 */
	public async link(id: number, sendRequest: boolean = true): Promise<void> {
		if (sendRequest) {
			try {
				this._storage.link(this._className, this._instance.Id, id);

				const instanceLink = await this._repository.getCollectionByIds([id]);
				await this._repository.createOrUpdate(instanceLink[0]);
			} catch (e) {
				await this.detach(id, false);
				throw e;
			}
			return;
		}

		this._storage.link(this._className, this._instance.Id, id);
	}

	/**
	 * returns an array of the of the linked item
	 */
	public async get(): Promise<T[]> {
		return await this._repository.getCollectionByIds(this.LinkedIds);
	}

	/**
	 * returns the list of linked ids
	 */
	public get LinkedIds() {
		return this._storage.getLinkedIds(this._className, this._instance.Id);
	}
}