import {AError} from './AError';

/**
 * @class UnauthorizedError
 * throw this error if a user is not authenticated
 */
export class UnauthorizedError extends AError {
	constructor(message: string = 'Not authenticated') {
		super(message);
	}
}