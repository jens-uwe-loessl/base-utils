/**
 * @class AError
 * basic error class, all errors needs to extend this class
 */
export abstract class AError extends Error {
	constructor(message?: string) {
		super(message);

		// Set the prototype explicitly.

		// @ts-ignore
		this.__proto__ = new.target.prototype;
	}
}