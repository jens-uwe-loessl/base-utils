import {AError} from './AError';

/**
 * @class UnsupportedMethodCallError
 * throw when calling a message is not allowed at the moment
 */
export class UnsupportedMethodCallError extends AError {
	constructor(message: string = 'Calling this method is not allowed in the current status.') {
		super(message);
	}
}