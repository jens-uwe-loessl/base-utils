/**
 * @class MapUtils
 * collection of static helper functions according to maps
 */
export class MapUtils {
	/**
	 * merges two Maps and return the merged value as new instance
	 * @param map
	 * @param mapToMerge
	 * @return Map<T,s>
	 */
	public static merge<T, S>(map: Map<T, S>, mapToMerge: Map<T, S>): Map<T, S> {
		return new Map(function* () {
			yield* map;
			yield* mapToMerge;
		}());
	}
}