/**
 * @class Generator
 * generates UUIDs
 */
export class Generator {
	/**
	 * generates a random id with the given length
	 * @param length - length of the id
	 */
	public static createUUID(length: number): string {
		const symbols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];

		return new Array(length).fill(undefined).map(() => symbols[Math.floor(Math.random() * symbols.length)]).join('');
	}
}