/**
 * @class ArrayUtils
 * helper functions for arrays
 */
export class ArrayUtils {
	/**
	 * returns a new array and unify it by the value of a given key
	 * it removes the next n found elements when the value is the same
	 * @param collection - list that needs to be unified
	 * @param key - key of the element
	 */
	public static unifyByKey<T, K extends keyof T>(collection: T[], key: K): T[] {
		const set = new Set<T[K]>();
		return collection.filter((item) => {
			const found = set.has(item[key]);
			set.add(item[key]);
			return !found;
		});
	}
}