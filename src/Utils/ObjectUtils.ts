/**
 * @class ObjectUtils
 * collection of helper functions according to objects
 */
export class ObjectUtils {
	/**
	 * clones the object, and takes all serializable properties with, does not work on instances
	 * @param object - can be anything
	 */
	public static deepCopy<T>(object: T): T {
		return JSON.parse(JSON.stringify(object));
	}

	/**
	 * check if to objects equals
	 * @param obj1
	 * @param obj2
	 * @returns {boolean}
	 */
	public static objectsEqual(obj1: any, obj2: any): boolean {
		let a = JSON.stringify(obj1) || '',
			b = JSON.stringify(obj2) || '';

		return (a.split('').sort().join('') === b.split('').sort().join(''));
	}
}