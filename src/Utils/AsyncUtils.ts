/**
 * @class AsyncUtils
 * collection of helper functions according to promises and async behaviour
 */
export class AsyncUtils {
	/**
	 * wait for the given time
	 * @param ms - time to wait in milli seconds
	 */
	public static wait(ms: number): Promise<void> {
		return new Promise((resolve) => {
			setTimeout(resolve, ms);
		});
	}
}