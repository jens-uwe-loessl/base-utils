/**
 * @class NumberUtils
 * collection of helper functions according to numbers
 */
export class NumberUtils {

	/**
	 * parses a number from a string
	 * @param s - string to parse
	 * @param decimalSeparator - decimal separator
	 */
	public static parse(s: string, decimalSeparator: string): number {
		return parseFloat(s.replace(decimalSeparator, '.'));
	}

	/**
	 * converts a number to string with the given separator
	 * @param n - number to convert
	 * @param decimalSeparator - decimal separator
	 * @param decimalNumbers - number of decimal places
	 */
	public static toString(n: number, decimalSeparator: string, decimalNumbers: number = 2): string {
		return n.toFixed(decimalNumbers).replace('.', decimalSeparator);
	}
}