/**
 * own implementation of a deferred object
 */
export class Deferred<T> {
	private readonly _promise: Promise<T>;

	// @ts-ignore - it is assigned in the promise constructor
	private _resolveFn: (value: T) => void;
	// @ts-ignore - it is assigned in the promise constructor
	private _rejectFn: (error: Error) => void;
	private _state: EPromiseState = EPromiseState.PENDING;

	constructor() {
		this._promise = new Promise((resolve, reject) => {
			this._resolveFn = resolve;
			this._rejectFn = reject;
		});
	}

	public resolve(value: T): void {
		this._resolveFn(value);
		this._state = EPromiseState.RESOLVED;
	}

	public reject(error: Error): void {
		this._rejectFn(error);
		this._state = EPromiseState.REJECTED;
	}

	public get Resolved(): boolean {
		return this._state === EPromiseState.RESOLVED;
	}

	public get Rejceted(): boolean {
		return this._state === EPromiseState.REJECTED;
	}

	public get Promise(): Promise<T> {
		return this._promise;
	}
}

enum EPromiseState {
	PENDING, RESOLVED, REJECTED
}
