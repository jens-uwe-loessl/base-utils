import './jest.d';

expect.extend({
	/**
	 * check if a number is around another number
	 * @param actual - the actual value
	 * @param expected - the expected value
	 * @param precision - precision of the matcher defined as half of power of 10
	 * e.g. -1 equals -+ 5, 0 equals -+ 0.5
	 */
	toBeAround(actual, expected, precision = 2) {
		const pass = Math.abs(expected - actual) < Math.pow(10, -precision) / 2;
		if (pass) {
			return {
				message: () => `expected ${actual} not to be around ${expected}`,
				pass: true
			};
		} else {
			return {
				message: () => `expected ${actual} to be around ${expected}`,
				pass: false
			};
		}
	}
});
