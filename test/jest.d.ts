declare namespace jest {
	// tslint:disable-next-line:interface-name because its not our module
	interface Matchers<R> {
		/**
		 * check if a number is around another number
		 * @param value - the expected value
		 * @param precision - precision of the matcher defined as half of power of 10
		 * e.g. -1 equals -+ 5, 0 equals -+ 0.5
		 */
		toBeAround(value: number, precision: number): CustomMatcherResult;
	}
}