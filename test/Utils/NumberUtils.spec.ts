import {NumberUtils} from '../../src/Utils/NumberUtils';

describe('NumberUtils', () => {
	describe('parse', () => {
		it('parses a string and returns a number', () => {
			expect(NumberUtils.parse('1,5', ',')).toEqual(1.5);
		});
	});

	describe('toString', () => {
		it('converts a number and returns a string', () => {
			expect(NumberUtils.toString(1.5, ',')).toEqual('1,50');
		});

		it('converts a number and returns a string without decimal separator when there is no decimal number', () => {
			expect(NumberUtils.toString(1, ',', 0)).toEqual('1');
		});
	});
});