import {ArrayUtils} from '../../src/Utils/ArrayUtils';

describe('ArrayUtils', () => {
	describe('unifyByKey', () => {
		it('does nothing with an empty array', () => {
			expect(ArrayUtils.unifyByKey([], '')).toEqual([]);
		});

		it('does nothing with one element', () => {
			expect(ArrayUtils.unifyByKey([{k: 1}], 'k')).toEqual([{k: 1}]);
		});

		it('removes 1 duplicated element', () => {
			expect(ArrayUtils.unifyByKey([{k: 1}, {k: 1}], 'k')).toEqual([{k: 1}]);
		});

		it('removes 2 duplicated elements', () => {
			expect(ArrayUtils.unifyByKey([{k: 1}, {k: 1}, {k: 1}], 'k')).toEqual([{k: 1}]);
		});

		it('removes 1 duplicated element and leaves the rest', () => {
			expect(ArrayUtils.unifyByKey([{k: 1}, {k: 2}, {k: 1}], 'k')).toEqual([{k: 1}, {k: 2}]);
		});
	});
});