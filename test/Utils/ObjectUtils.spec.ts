import {ObjectUtils} from '../../src/Utils/ObjectUtils';

describe('ObjectUtils', () => {
	describe('makeDeepCopy', () => {
		it('Object should be the same but no reference', () => {
			const dummyObject = {
					property: 'key',
					property2: {
						property21: 'key',
						property22: 'key'
					}
				},
				deepCopy = ObjectUtils.deepCopy(dummyObject);

			// properties are the same
			expect(deepCopy).toEqual(dummyObject);

			// it is no reference
			expect(deepCopy).not.toBe(dummyObject);
		});
	});

	describe('objectsEqual', () => {
		it('return true when undefined|undefined', () => {
			expect(ObjectUtils.objectsEqual(undefined, undefined)).toBe(true);
		});

		it('return false when undefined|null', () => {
			expect(ObjectUtils.objectsEqual(undefined, null)).toBe(false);
		});

		it('return false when NaN|undefined', () => {
			expect(ObjectUtils.objectsEqual(NaN, undefined)).toBe(false);
		});

		it('return true when []|[]', () => {
			expect(ObjectUtils.objectsEqual([], [])).toBe(true);
		});

		it('return false when {}|null', () => {
			expect(ObjectUtils.objectsEqual({}, null)).toBe(false);
		});

		it('return true when {}|{}', () => {
			expect(ObjectUtils.objectsEqual({}, {})).toBe(true);
		});

		it('return true when {p1: {p1: \'k\'}}|{p1: {p1: \'k\'}}', () => {
			expect(ObjectUtils.objectsEqual({p1: {p1: 'k'}}, {p1: {p1: 'k'}})).toBe(true);
		});

		it('return true when {p1: {p1: \'k\'}}|{p1: {p1: \'l\'}}', () => {
			expect(ObjectUtils.objectsEqual({p1: {p1: 'k'}}, {p1: {p1: 'l'}})).toBe(false);
		});
	});
});
