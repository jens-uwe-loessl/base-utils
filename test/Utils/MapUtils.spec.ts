import {MapUtils} from '../../src/Utils/MapUtils';

describe('MapUtils', () => {
	describe('merge', () => {
		it('merges two instances of a Map', () => {
			const map1 = new Map();
			map1.set('a', 'foo');

			const map2 = new Map();
			map2.set('b', 'bar');

			const result = MapUtils.merge(map1, map2);

			expect(result.has('a')).toBe(true);
			expect(result.get('a')).toBe('foo');
			expect(result.has('b')).toBe(true);
			expect(result.get('b')).toBe('bar');
		});
	});
});