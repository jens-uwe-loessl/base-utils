import {Generator} from '../../src/Utils/Generator';

describe('Generator', () => {
	describe('generatorUUID', () => {
		const length = 50;

		it('generates an id with a given length', () => {
			expect(Generator.createUUID(length).length).toBe(length);
		});

		it('generates som kind of unique ids', () => {
			expect(Generator.createUUID(length)).not.toBe(Generator.createUUID(length));
		});
	});
});