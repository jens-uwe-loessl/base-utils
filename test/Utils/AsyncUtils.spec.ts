import {AsyncUtils} from '../../src/Utils/AsyncUtils';
import '../Matcher';

describe('AsyncUtils', () => {
	describe('wait', () => {
		it('merges two instances of a Map', async () => {
			const waitingTime = 50;

			const timeStart = Date.now();
			await AsyncUtils.wait(waitingTime);
			const timeEnd = Date.now();

			expect(timeEnd - timeStart).toBeAround(waitingTime, -1);
		});
	});
});