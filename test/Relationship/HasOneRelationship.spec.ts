import {HasOneRelationship} from '../../src/Relationship/HasOneRelationship';

describe('HasOneRelationship', () => {
	let relationship;

	const dummyInstanceOne = {Id: 1};
	const repoInstanceTwo = {
		createOrUpdate: (id: { Id: number }) => Promise.resolve(id),
		getById: (id: number) => Promise.resolve({Id: id}),
		getCollectionByIds: (ids: number[]) => Promise.resolve(ids.map((id) => ({Id: id})))
	};

	const before = () => {
		relationship = new HasOneRelationship(dummyInstanceOne, repoInstanceTwo, 'one', 'two');
	};

	describe('constructor', () => {
		beforeEach(before);

		it('goes through without error', () => {
			relationship = new HasOneRelationship(dummyInstanceOne, repoInstanceTwo, 'one', 'two');
		});

		it('elements are linked when added in constructor', () => {
			relationship = new HasOneRelationship(dummyInstanceOne, repoInstanceTwo, 'one', 'two', 1);
			expect(relationship.LinkedId).toEqual(1);
		});
	});

	describe('detach', () => {
		it('detaches linked object', async (done) => {
			try {
				relationship = new HasOneRelationship(dummyInstanceOne, repoInstanceTwo, 'one', 'two', 1);
				await relationship.detach(1);
				expect(relationship.LinkedId).toEqual(undefined);
				done();
			} catch (e) {
				done.fail(e);
			}
		});
	});

	describe('get', () => {
		it('returns linked instance', async (done) => {
			try {
				relationship = new HasOneRelationship(dummyInstanceOne, repoInstanceTwo, 'two', 'three', 1);
				expect(await relationship.get()).toEqual({Id: 1});
				done();
			} catch (e) {
				done.fail(e);
			}
		});
	});

	describe('link', () => {
		it('links two instance', async (done) => {
			try {
				relationship = new HasOneRelationship(dummyInstanceOne, repoInstanceTwo, 'two', 'three');
				relationship.link(1);
				expect(await relationship.get()).toEqual({Id: 1});
				done();
			} catch (e) {
				done.fail(e);
			}
		});
	});
});