import {HasManyRelationship} from '../../src/Relationship/HasManyRelationship';

describe('HasManyRelationship', () => {
	let relationship;

	const dummyInstanceOne = {Id: 1};
	const repoInstanceTwo = {
		createOrUpdate: (id: { Id: number }) => Promise.resolve(id),
		getById: (id: number) => Promise.resolve({Id: id}),
		getCollectionByIds: (ids: number[]) => Promise.resolve(ids.map((id) => ({Id: id})))
	};

	const before = () => {
		relationship = new HasManyRelationship(dummyInstanceOne, repoInstanceTwo, 'one', 'two');
	};

	describe('constructor', () => {
		beforeEach(before);

		it('goes through without error', () => {
			relationship = new HasManyRelationship(dummyInstanceOne, repoInstanceTwo, 'one', 'two');
		});

		it('elements are linked when added in constructor', () => {
			relationship = new HasManyRelationship(dummyInstanceOne, repoInstanceTwo, 'one', 'two', [1]);
			expect(relationship.LinkedIds).toEqual([1]);
		});
	});

	describe('detach', () => {
		it('detaches linked object', async (done) => {
			relationship = new HasManyRelationship(dummyInstanceOne, repoInstanceTwo, 'one', 'two', [1]);
			await relationship.detach(1);
			expect(relationship.LinkedIds).toEqual([]);
			done();
		});

		it('detaches only  the linked object', async (done) => {
			relationship = new HasManyRelationship(dummyInstanceOne, repoInstanceTwo, 'one', 'two', [1, 2]);
			await relationship.detach(1);
			expect(relationship.LinkedIds).toEqual([2]);
			done();
		});
	});

	describe('detachAll', () => {
		it('detaches linked object', async (done) => {
			relationship = new HasManyRelationship(dummyInstanceOne, repoInstanceTwo, 'one', 'two', [1]);
			await relationship.detachAll();
			expect(relationship.LinkedIds).toEqual([]);
			done();
		});

		it('detaches only  the linked object', async (done) => {
			relationship = new HasManyRelationship(dummyInstanceOne, repoInstanceTwo, 'one', 'two', [1, 2]);
			await relationship.detachAll();
			expect(relationship.LinkedIds).toEqual([]);
			done();
		});
	});

	describe('get', () => {
		it('returns linked instances', async () => {
			relationship = new HasManyRelationship(dummyInstanceOne, repoInstanceTwo, 'two', 'three', [1]);
			expect(await relationship.get()).toEqual([{Id: 1}]);
		});
	});
});