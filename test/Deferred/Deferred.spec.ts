import {Deferred} from '../../src/Deferred/Deferred';

describe('Deferred', () => {
	describe('constructor', () => {
		it('constructor returns the right instance', () => {
			const deferred = new Deferred();

			expect(deferred instanceof Deferred).toBe(true);
			expect(deferred.Resolved).toBe(false);
			expect(deferred.Rejceted).toBe(false);
		});
	});

	describe('resolve', () => {
		it('resolves the promise', async (done) => {
			const deferred: Deferred<void> = new Deferred();
			deferred.resolve(undefined);

			await deferred.Promise;
			expect(deferred.Resolved).toBe(true);
			expect(deferred.Rejceted).toBe(false);

			done();
		});
	});

	describe('reject', () => {
		it('rejects the promise', async (done) => {
			const deferred: Deferred<void> = new Deferred();
			deferred.reject(new Error());

			try {
				await deferred.Promise;
				done.fail();
			} catch (e) {
				expect(deferred.Resolved).toBe(false);
				expect(deferred.Rejceted).toBe(true);
				done();
			}
		});
	});
});