import {UnsupportedMethodCallError} from '../../src/Error/UnsupportedMethodCallError';

describe('UnsupportedMethodCallError', () => {
	describe('constructor', () => {
		it('instanceOf check retrieves the expected result', () => {
			const error = new UnsupportedMethodCallError();

			expect(error instanceof UnsupportedMethodCallError).toBe(true);
		});
	});
});