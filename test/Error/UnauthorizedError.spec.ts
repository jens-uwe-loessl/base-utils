import {UnauthorizedError} from '../../src/Error/UnauthorizedError';

describe('UnauthorizedError', () => {
	describe('constructor', () => {
		it('instanceOf check retrieves the expected result', () => {
			const error = new UnauthorizedError();

			expect(error instanceof UnauthorizedError).toBe(true);
		});
	});
});