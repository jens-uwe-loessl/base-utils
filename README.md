# base-utils

This project includes a collection of basic functions that are project independent, and are helpful in different cases.
In detail, it includes:

- ArrayUtils
- AsyncUtils
- Deferred
- Errors
- Generator
  - to generate uuids
- NumberUtils
- MapUtils
- ObjectUtils
- Entity relationship implementation

## Scripts

- Run tests: _yarn run test_
- Run test watcher _yarn run test:dev_
- Run build: _yarn run build_ 